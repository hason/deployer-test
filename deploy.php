<?php
require 'recipe/composer.php';

server('production-server', '77.93.202.253', 5900)
    ->path('/var/www/test/prod')
    ->user('root')
  //->user('name', 'password')
;

server('development-server', '77.93.202.253', 5900)
    ->path('/var/www/test/dev')
    ->user('root')
    //->pubKey('~/.ssh/id_rsa.pub', '~/.ssh/id_rsa');
    ->pubKey()
    //->user('name', 'password')
;

// Specify repository from which to download your projects code.
// Server has to be able clone your project from this repository.
set('repository', 'https://bitbucket.org/hason/deployer-test');

stage('development', array('development-server'), array('branch'=>'master'), true);
stage('production', array('production-server'), array('branch'=>'master'));

task('php-fpm:restart', function () {
    run("sudo service php5-fpm restart");
})->description('Starting PHP5-FPM');

after('deploy:end', 'php-fpm:restart');
