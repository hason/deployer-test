<?php

umask(0000);

require __DIR__.'/../vendor/autoload.php';
require __DIR__.'/../app/AppKernel.php';

function homepage()
{
    return new \Symfony\Component\HttpFoundation\Response('Homepage');
}

$kernel = new AppKernel('dev', true);
$request = \Symfony\Component\HttpFoundation\Request::createFromGlobals();
$response = $kernel->handle($request);
$response->send();
$kernel->terminate($request, $response);
